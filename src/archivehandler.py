#!/usr/bin/env python3


import rarfile
from rarfile import RarFile


def extract_rar(filename, password):
	try:
		rar = RarFile(filename)
		rar.extractall(pwd=password)
		return True
	except:
		return False
