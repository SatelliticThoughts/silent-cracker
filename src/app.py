#!/usr/bin/env python3


import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

import uimain
from uimain import *


def main():
	win = UIMain()
	win.show()
	Gtk.main()

if __name__ == "__main__":
	main()
