#!/usr/bin/env python3


def gen_str(characters, codes):
	gen_str = ""
	for code in codes:
		gen_str += characters[code]
	return gen_str


def update_codes(char_len, codes):
	rev = codes[::-1]
	i = 0
	while True:
		rev[i] += 1
		if rev[i] == char_len:
			rev[i] = 0
			if i == len(rev) - 1:
				rev.append(0)
				break
			i += 1
		else:
			break			
	return rev[::-1]
	
