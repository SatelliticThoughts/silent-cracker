#!/usr/bin/env python3


import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject, GLib
import threading

import cracker as c
import archivehandler as a


GObject.threads_init()


class UIMain:
	def __init__(self):
		builder = Gtk.Builder()
		builder.add_from_file("main.glade")
		
		self.__win = builder.get_object("winMain")
		self.__entry_chars = builder.get_object("entryCharacters")
		self.__spin_btn = builder.get_object("spinBtn")
		self.__file_chooser = builder.get_object("fileChooser")
		self.__entry_path = builder.get_object("entryPath")
		self.__btn_crack = builder.get_object("btnCrack")
		self.__lbl_attempts = builder.get_object("lblAttempts")
		
		self.__win.connect("destroy", self.on_destroy)
		self.__entry_chars.connect("changed", 
				self.on_entryChars_changed)
		self.__spin_btn.connect("value-changed", 
				self.on_spinBtn_value_changed)
		self.__file_chooser.connect("file-set", 
				self.on_fileChooser_file_set)
		self.__entry_path.connect("activate", self.on_entryPath_activate)
		self.__btn_crack.connect("clicked", self.on_btnCrack_clicked)
		
		self.__chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
		self.__entry_chars.set_text(self.__chars)
		self.__starting_size = 1
		
	def show(self):
		self.__win.show_all()

	def on_destroy(self, *args):
		Gtk.main_quit()
		
	def on_entryChars_changed(self, *args):
		self.__chars = self.__entry_chars.get_text()
	
	def on_spinBtn_value_changed(self, *args):
		self.__starting_size = int(self.__spin_btn.get_text())
		
	def on_fileChooser_file_set(self, *args):
		self.__filename = args[0].get_filename()
		self.__entry_path.set_text(self.__filename)
		
	def on_entryPath_activate(self, *args):
		self.__filename = args[0].get_text()
		self.__file_chooser.set_filename(self.__filename)
	
	def on_btnCrack_clicked(self, *args):
		try:
			thread = threading.Thread(target=self.crack)
			thread.daemon = True
			thread.start()
		except Exception as e:
			print(e)
		
	def crack(self):
		codes = []
		for i in range(0, self.__starting_size):
			codes.append(0)
			
		found = False
		i = 1
		while not found:
			password = c.gen_str(self.__chars, codes)
			codes = c.update_codes(len(self.__chars), codes)
			text = "Attempts: {0}; {1}".format(i, password)
			try:
				GLib.idle_add(self.__lbl_attempts.set_text, text)
			except Exception as e:
				print(e)
			i += 1
			found = a.extract_rar(self.__filename, password)
			

	def update_label(self, attempts, password):
		text = "Attempts: {0}; {1}".format(attempts, password)
		self.__lbl_attempts.set_text(text)
		
